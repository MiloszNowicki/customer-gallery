import React from 'react';
import { GalleryWrapper } from '../components/photos-data'


export class GalleryHOC extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            config: {
                mapping: {
                    fullscreen_photos_uri: [],
                    desktop_photos_uri: [],
                    mobile_photos_uri: [],
                    thumbnail_photos_uri: [],
                },
                offer_number:'',
                photos_description:'',
            }
        }
    }

    getOfferData() {
        let axios = require('axios');
        axios.get('http://localhost:8080/db.json')
            .then(response => this.mapPhotoUriToState(response));
    }

    getArrayOfPhotosUri(offerPhotos) {
    return offerPhotos.map(offerPhoto => offerPhoto.uri_path);
    }

    mapPhotoUriToState(response) {
        let arrayOfPhotoVariants = response.data.photos;
        this.setState ({
            config: {
                mapping: {
                    fullscreen_photos_uri: this.getArrayOfPhotosUri(arrayOfPhotoVariants.offer_fullscreen),
                    desktop_photos_uri   : this.getArrayOfPhotosUri(arrayOfPhotoVariants.offer_preview),
                    mobile_photos_uri    : this.getArrayOfPhotosUri(arrayOfPhotoVariants.offer_iq),
                    thumbnail_photos_uri : this.getArrayOfPhotosUri(arrayOfPhotoVariants.offer_thumbnail),
                },
                offer_number: "666",
                photos_description: "none so far",
                prefixUrl:"http://s.qarson.com/"
            }
        })
    }

    componentDidMount() {
        this.getOfferData();
        // this.mapPhotoUriToState(response);
    }

    render() {
        return (
            <GalleryWrapper
                config = {this.state.config}
            />
        )
    }
}
