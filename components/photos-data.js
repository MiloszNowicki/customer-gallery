import React, {Component} from 'react';
import ImageGallery from 'react-image-gallery';
import PropTypes from 'prop-types';
import Gallery from '../render/gallery'


export class GalleryWrapper extends React.Component {

    groupPhotoVariants() {
        let fullscreeenPhotoVariants = this.props.config.mapping.fullscreen_photos_uri;
        let desktopPhotosVariants    = this.props.config.mapping.desktop_photos_uri;
        let mobilePhotoVariants      = this.props.config.mapping.mobile_photos_uri;
        let thumbnailPhotoVariants   = this.props.config.mapping.thumbnail_photos_uri;
        let groupedPhotoVariants     = desktopPhotosVariants
            .map( desktopPhotoUri =>
                this.filterPhotoVariant (
                    desktopPhotoUri,
                    fullscreeenPhotoVariants,
                    desktopPhotosVariants,
                    mobilePhotoVariants,
                    thumbnailPhotoVariants
                )
            );
        console.log(groupedPhotoVariants);
        return groupedPhotoVariants;
    }

    filterPhotoVariant (uriToMatch, ...collectionOfPhotoVariants) {
        let photoShotCollection = {};
        collectionOfPhotoVariants.forEach(photoVariants => {
            let matchingVariantSizeUri =
                photoVariants.find(
                    photoVariant  => this.substractPhotoShotType(photoVariant, 0) === this.substractPhotoShotType(uriToMatch, 0)
                );
            photoShotCollection[this.substractPhotoSizeType(matchingVariantSizeUri).toString()] = matchingVariantSizeUri;

        });
        return photoShotCollection;
    }


    substractPhotoShotType(photoUrl, fromOrToDot) {
        return photoUrl.substr(fromOrToDot, photoUrl.indexOf('.'));
    }

    substractPhotoSizeType(photoUrl) {
        return photoUrl.substr( (photoUrl.indexOf('.') + 1) , ( photoUrl.lastIndexOf('.') - photoUrl.indexOf('.') - 1 ) );
    }

    createArrayOfGalleryObjects (photoShotCollections, preOfferUri) {
        return photoShotCollections.map(photoShotCollection => ({
            thumbnail:preOfferUri +  photoShotCollection.q,
            imageSet: [

                {
                    srcSet: preOfferUri +  photoShotCollection.iq,
                    media: '(max-width: 639px)',
                },
                {
                    srcSet: preOfferUri +  photoShotCollection.qop,
                    media: '(max-width: 1199px)',
                },
                {
                    srcSet: preOfferUri + photoShotCollection.qhd,
                    media: '(min-width: 1200px)',
                },
            ]
        }));
    }

    componentDidUpdate() {
        //let arr = this.groupPhotoVariants();
        //console.log(this.createArrayOfGalleryObjects(this.groupPhotoVariants(), this.props.config.prefixUrl));
    }

    render() {
        return(
            <Gallery images ={this.createArrayOfGalleryObjects(this.groupPhotoVariants(), this.props.config.prefixUrl)} />
        )
    }
}

GalleryWrapper.propTypes = {
    config: PropTypes.object,
    // config : {
    //     mapping: {
    //         fullscreen_photos_uri: PropTypes.array,
    //         desktop_photos_uri: PropTypes.array,
    //         mobile_photos_uri: PropTypes.array,
    //         thumbnail_photos_uri: PropTypes.array,
    //     },
    //     offer_number: PropTypes.string,
    //     photo_description: PropTypes.string,
    // }
}