import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';
import "../node_modules/react-image-gallery/styles/css/image-gallery.css";
import { PhotosData } from  "../components/photos-data"
import {GalleryHOC} from "../containers/GalleryHOC";
//import PropTypes from 'prop-types';


const Gallery = (props) => {
    // constructor(props) {
    //     super(props);
    //     this.state ={
    //         images : [],
    //     }
    // }

         return(
             <ImageGallery
                 showPlayButton = {false}
                 items = {props.images}
                 additionalClass = "gallery_section-wrapper"
             />
         );

}

// Gallery.propTypes = {
//     images : PorpTypes.array,
// }

export default Gallery;