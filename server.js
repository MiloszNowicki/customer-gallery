//const express = require('express');
const webpack = require('webpack');
const webpackDevServer = require('webpack-dev-server');

//const app = express();
const config = require('./webpack.config.js');
const compiler = webpack(config);
const options = {
            hot: true,
};
webpackDevServer.addDevServerEntrypoints(config, options);
const server  = new webpackDevServer(compiler, options);

server.listen(3000, function() {
    console.log('example app listening on port 3000!\n')
});